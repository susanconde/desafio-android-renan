package renankirk.concretesolutions.gitrepositories;

import com.google.inject.AbstractModule;

import renankirk.concretesolutions.gitrepositories.domain.bo.RepositoryBO;
import renankirk.concretesolutions.gitrepositories.domain.bo.RepositoryPRBO;
import renankirk.concretesolutions.gitrepositories.domain.bo.impl.RepositoryBOImpl;
import renankirk.concretesolutions.gitrepositories.domain.bo.impl.RepositoryPRBOImpl;
import renankirk.concretesolutions.gitrepositories.presenter.RepositoryPresenter;
import renankirk.concretesolutions.gitrepositories.presenter.impl.RepositoryPresenterImpl;

/**
 * Created by RenanKirk on 17/05/2016.
 */
public class BindingModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(RepositoryPresenter.class).to(RepositoryPresenterImpl.class);
        bind(RepositoryBO.class).to(RepositoryBOImpl.class);
        bind(RepositoryPRBO.class).to(RepositoryPRBOImpl.class);
    }
}
