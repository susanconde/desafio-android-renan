package renankirk.concretesolutions.gitrepositories.domain.bo.impl;

import renankirk.concretesolutions.gitrepositories.domain.model.RepositoryPR;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by RenanKirk on 18/05/2016.
 */
public abstract class AbstractBOImpl<T> implements Callback<T> {

    protected Retrofit getRetrofit(){
        return new Retrofit.Builder()
                .baseUrl("https://api.github.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if(response.isSuccessful()){
            showSuccess(response.body());
        } else {
            showFail(response.message());
        }

    }

    protected abstract void showFail(String message);

    protected abstract void showSuccess(T body);

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        showFail(t.getMessage());
    }

}
