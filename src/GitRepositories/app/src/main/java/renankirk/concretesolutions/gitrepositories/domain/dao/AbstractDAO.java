/*
 * Copyright (c) 2014 Samsung Electronics Co., Ltd.
 * All rights reserved.
 *
 * This software is a confidential and proprietary information of Samsung
 * Electronics, Inc. ("Confidential Information").  You shall not disclose such
 * Confidential Information and shall use it only in accordance with the terms
 * of the license agreement you entered into with Samsung Electronics.
 */
package renankirk.concretesolutions.gitrepositories.domain.dao;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.google.inject.Inject;

import java.io.File;

public class AbstractDAO {

    private static final String PREF_NAME = "CACHE_PREFERENCES";

    private static final String SHARED_PREFERENCE = "/shared_prefs/";

    @Inject
    private Context context;

    /**
     * Get the Editor instance for Preferences named PRE_NAME using private
     * mode.
     */
    public Editor getEditor() {
        return this.context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
                .edit();
    }

    /**
     * Get Shared Preferences with name PREF_name using MODE_PRIVATE
     */
    public SharedPreferences getSharedPreferences() {
        return this.context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    /**
     * clear all preference.
     *
     * @return
     */
    public void clearPreference() {
        getEditor().clear().commit();
    }

}
