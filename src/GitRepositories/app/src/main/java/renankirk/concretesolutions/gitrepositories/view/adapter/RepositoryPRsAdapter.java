package renankirk.concretesolutions.gitrepositories.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.inject.Inject;

import java.text.SimpleDateFormat;
import java.util.List;

import renankirk.concretesolutions.gitrepositories.R;
import renankirk.concretesolutions.gitrepositories.domain.model.RepositoryPR;
import renankirk.concretesolutions.gitrepositories.presenter.RepositoryPresenter;

/**
 * Created by RenanKirk on 17/05/2016.
 */
public class RepositoryPRsAdapter extends RecyclerView.Adapter<RepositoryPRsAdapter.RepositoryPRViewHolder>{

    private List<RepositoryPR> prs;

    @Inject
    private Context context;

    @Inject
    private RepositoryPresenter presenter;

    @Override
    public RepositoryPRViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_repository_pr_item, parent, false);
        RepositoryPRViewHolder vh = new RepositoryPRViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(RepositoryPRViewHolder holder, int position) {
        final RepositoryPR pr = prs.get(position);
        holder.title.setText(pr.getTitle());
        holder.desc.setText(pr.getBody());
        SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy - hh:mm");
        holder.date.setText(dt.format(pr.getCreated_at()).toString());
        holder.name.setText(pr.getUser().getLogin());

        Glide.with(context).load(pr.getUser().getAvatar_url()).asBitmap().into(holder.photo);
    }

    @Override
    public int getItemCount() {
        return prs.size();
    }

    public void setPRs(List<RepositoryPR> prs) {
        this.prs = prs;
    }

    public class RepositoryPRViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView title;
        TextView desc;
        TextView date;
        ImageView photo;
        TextView name;
        public RepositoryPRViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.txv_pr_title);
            desc = (TextView) itemView.findViewById(R.id.txv_pr_desc);
            date = (TextView) itemView.findViewById(R.id.txv_pr_date);
            photo = (ImageView) itemView.findViewById(R.id.iv_pr_owner_photo);
            name = (TextView) itemView.findViewById(R.id.txv_pr_owner_name);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(prs.get(getPosition()).getHtml_url()));
            context.startActivity(intent);
        }
    }
}
